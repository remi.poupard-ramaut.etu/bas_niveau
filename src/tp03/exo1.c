#include <stdlib.h>
#include <stdio.h>

typedef struct {
    char* tableau;
    int taille;
} super_tableau_t;

typedef super_tableau_t chaine_t;

char* nouveau_tableau (int taille);
void initialise_tableau (char* tableau, int taille, char car);
void affiche_tableau (char* tableau, int taille);
void liberation_du_tableau (char* tableau);
void place_dans_tableau (char* tableau, int taille, int indice, char car);
char lecture_du_tableau (char* tableau, int taille, int indice);
super_tableau_t* nouveau_super_tableau (int taille);
void initialise_super_tableau (super_tableau_t* super_tab, char car);
void affiche_super_tableau (super_tableau_t* super_tab);
void liberation_du_super_tableau (super_tableau_t* super_tab);
void place_dans_super_tableau (super_tableau_t* super_tab, int indice, char car);
char lecture_du_super_tableau (super_tableau_t* super_tab, int indice);
int taille_super_tableau(super_tableau_t* super_tab);
int chaine_est_valide (chaine_t* chaine);
int taille_de_la_chaine (chaine_t* chaine);
void affiche_chaine (chaine_t* chaine);
void ajoute_caractere (chaine_t* chaine, char car);
void allonge_tableau_de_la_chaine (chaine_t* chaine);
void ajoute_caractere (chaine_t* chaine, char car);
void ajoute_chaine (chaine_t* chaine1, chaine_t* chaine2);
void lire_entree_utilisateur (chaine_t* chaine);

int main()
{/* EXO1
    printf("/////q1/////\n");
    char* tab;
    tab=nouveau_tableau(4);
    initialise_tableau(tab,4,'a');
    *(tab+2)='e';
    affiche_tableau(tab,4);
    liberation_du_tableau(tab);
    affiche_tableau(tab,4);
    printf("/////q2/////\n");
    initialise_tableau(tab,4,'a');
    place_dans_tableau(tab,4,1,'t');
    place_dans_tableau(tab,4,5,'u');
    printf("%c\n",lecture_du_tableau(tab,4,0));
    printf("%c\n",lecture_du_tableau(tab,4,6));
 */


/*  EXO2  
    printf("/////q1/////\n");
    super_tableau_t* super_tab;
    super_tab=nouveau_super_tableau(4);

    initialise_super_tableau(super_tab,'a');
    affiche_super_tableau(super_tab);

    place_dans_tableau(super_tab->tableau,super_tab->taille,2,'e');
    affiche_super_tableau(super_tab);

    printf("/////q2/////\n");

    initialise_super_tableau(super_tab,'a');
    place_dans_super_tableau(super_tab,1,'t');
    //place_dans_super_tableau(super_tab,5,'u');
    printf("%c\n",lecture_du_super_tableau(super_tab,0));
    //printf("%c\n",lecture_du_super_tableau(super_tab,6));
    affiche_super_tableau(super_tab);

    printf("%d\n",taille_super_tableau(super_tab));

    liberation_du_super_tableau(super_tab);
 */
    super_tableau_t* chaine;
    super_tableau_t* chaine2;
    chaine=nouveau_super_tableau(4);
    initialise_super_tableau(chaine,'a');
    printf("taille_de_la_chaine : %d\n",taille_de_la_chaine(chaine));
    printf("affiche chaine\n");
    affiche_chaine(chaine);
    place_dans_tableau(chaine->tableau,chaine->taille,2,'\0');
    printf("chaine_est_valide : %d\n",chaine_est_valide(chaine));
    printf("taille_de_la_chaine : %d\n",taille_de_la_chaine(chaine));
    printf("affiche chaine\n");
    affiche_chaine(chaine);


    chaine=nouveau_super_tableau(10);
    place_dans_tableau(chaine->tableau,chaine->taille,0,'t');
    place_dans_tableau(chaine->tableau,chaine->taille,1,'o');
    place_dans_tableau(chaine->tableau,chaine->taille,2,'t');
    place_dans_tableau(chaine->tableau,chaine->taille,3,'\0');
    place_dans_tableau(chaine->tableau,chaine->taille,4,'a');
    affiche_chaine(chaine);
    ajoute_caractere(chaine,'o');
    affiche_chaine(chaine);

    chaine=nouveau_super_tableau(4);
    place_dans_tableau(chaine->tableau,chaine->taille,0,'t');
    place_dans_tableau(chaine->tableau,chaine->taille,1,'o');
    place_dans_tableau(chaine->tableau,chaine->taille,2,'t');
    place_dans_tableau(chaine->tableau,chaine->taille,3,'\0');
    printf("taille_de_la_chaine : %d\n",taille_de_la_chaine(chaine));
    affiche_chaine(chaine);
    ajoute_caractere(chaine,'o');
    printf("taille_de_la_chaine : %d\n",taille_de_la_chaine(chaine));
    affiche_chaine(chaine);


    chaine=nouveau_super_tableau(1);
    initialise_super_tableau(chaine,'\0');
    ajoute_caractere(chaine,'a');
    affiche_chaine(chaine);
    printf("taille_de_la_chaine : %d\n",taille_de_la_chaine(chaine));
    ajoute_caractere(chaine,'b');
    affiche_chaine(chaine);
    printf("taille_de_la_chaine : %d\n",taille_de_la_chaine(chaine));
    ajoute_caractere(chaine,'c');
    affiche_chaine(chaine);
    printf("taille_de_la_chaine : %d\n",taille_de_la_chaine(chaine));
    ajoute_caractere(chaine,'d');
    affiche_chaine(chaine);
    printf("taille_de_la_chaine : %d\n",taille_de_la_chaine(chaine));


    printf("ajoute chaine\n");

    chaine=nouveau_super_tableau(3);
    initialise_super_tableau(chaine,'a');
    place_dans_super_tableau(chaine,2,'\0');
    chaine2=nouveau_super_tableau(3);
    initialise_super_tableau(chaine2,'b');
    place_dans_super_tableau(chaine2,2,'\0');

    ajoute_chaine(chaine,chaine2);

    affiche_chaine(chaine);

    liberation_du_super_tableau(chaine);
    liberation_du_super_tableau(chaine2);

    chaine=nouveau_super_tableau(1);
    lire_entree_utilisateur(chaine);
    affiche_chaine(chaine);

    liberation_du_super_tableau(chaine);
    liberation_du_super_tableau(chaine2);
    return 0;

}

char* nouveau_tableau (int taille){
    char* tab;
    tab = malloc(taille * sizeof(char));
    return tab;
}

void initialise_tableau (char* tableau, int taille, char car){
    for(int i=0;i<taille;i++){
        *(tableau+i)=car;
    }
}

void affiche_tableau (char* tableau, int taille){
    for(int i=0;i<taille;i++){
        printf("%c ",*(tableau+i));
    }
    printf("\n");
}

void liberation_du_tableau (char* tableau){
    free(tableau);
}

void place_dans_tableau (char* tableau, int taille, int indice, char car){
    if(indice>taille-1){
        printf("indice trop grand\n");
        exit(1);
    }
    *(tableau+indice)=car;
} 

char lecture_du_tableau (char* tableau, int taille, int indice){
    if(indice>taille-1){
        printf("indice trop grand\n");
        exit(1);
    }
    return *(tableau+indice);
}

super_tableau_t* nouveau_super_tableau (int taille){
    super_tableau_t* ret;
    char* tab;
    tab=nouveau_tableau(4);
    ret=malloc(sizeof(super_tableau_t));
    ret->taille=taille;
    ret->tableau=tab;
    return ret;
}

void initialise_super_tableau (super_tableau_t* super_tab, char car){
   initialise_tableau(super_tab->tableau,super_tab->taille,car);
}

void affiche_super_tableau (super_tableau_t* super_tab){
   affiche_tableau(super_tab->tableau,super_tab->taille);
}

void liberation_du_super_tableau (super_tableau_t* super_tab){
    free(super_tab->tableau);
    free(super_tab);
}

void place_dans_super_tableau (super_tableau_t* super_tab, int indice, char car){
    place_dans_tableau(super_tab->tableau,super_tab->taille,indice,car);
}

char lecture_du_super_tableau (super_tableau_t* super_tab, int indice){
    return lecture_du_tableau(super_tab->tableau,super_tab->taille,indice);
}

int taille_super_tableau(super_tableau_t* super_tab){
    return super_tab->taille -1;
}

int chaine_est_valide (chaine_t* chaine){
    int idx=0;
    while(idx<chaine->taille&&lecture_du_super_tableau(chaine,idx)!='\0'){
        idx++;
    }
    return idx<chaine->taille;
}

int taille_de_la_chaine (chaine_t* chaine){
    if(chaine_est_valide(chaine)==1){
        return taille_super_tableau(chaine);
    }
    return -1;
}

void affiche_chaine (chaine_t* chaine){
    if(chaine_est_valide(chaine)==1){
        printf("%s\n",chaine->tableau);
    }
}

void ajoute_caractere (chaine_t* chaine, char car){
    int fin_str=0;
    while(fin_str<chaine->taille&&lecture_du_super_tableau(chaine,fin_str)!='\0'){
        fin_str++;
    }
    if(lecture_du_super_tableau(chaine,(chaine->taille)-1)=='\0'){
        allonge_tableau_de_la_chaine(chaine);
    }
    place_dans_super_tableau(chaine,fin_str,car);
    place_dans_super_tableau(chaine,fin_str+1,'\0');
}

void allonge_tableau_de_la_chaine (chaine_t* chaine){
    chaine->taille += 1;
    char* tab = nouveau_tableau(chaine->taille);
    for (int i = 0; i < chaine->taille; ++i) {
        tab[i] = chaine->tableau[i];
    }
    tab[taille_de_la_chaine(chaine)] = '\0';
    liberation_du_tableau(chaine->tableau);
    chaine->tableau = tab;
}

void ajoute_chaine (chaine_t* chaine1, chaine_t* chaine2){
    for (int i = 0; i < chaine2->taille; ++i) {
        ajoute_caractere(chaine1,*(chaine2->tableau+i));
    }
}

void lire_entree_utilisateur (chaine_t* chaine){
    initialise_super_tableau(chaine,'\0');
    char c;
    while ((c = getchar()) != '\n') {
        ajoute_caractere(chaine, c);
    }
}