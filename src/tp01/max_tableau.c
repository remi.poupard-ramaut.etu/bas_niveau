#include <stdio.h>

int main(){
    int tableau[6] = {5,4,7,14,2,3};
    int a;
    int max=tableau[0];
    int idx=0;
    for(a=1;a<6;a++){
        if(tableau[a]>max){
            max=tableau[a];
            idx=a;
        }
    }
    printf("La valeur maximale est %d.\n",max);
    printf("Elle se trouve en position %d.\n",idx);
    return 0;
}