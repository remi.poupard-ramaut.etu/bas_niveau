#include <stdio.h>

int main(){
    int cpt;
    for(cpt=1;cpt<=30;cpt++){
        if(cpt%3 == 0&&cpt%5 != 0){
            printf("fizz\n");
        }else if(cpt%5 == 0&&cpt%3 != 0){
            printf("buzz\n");
        }else if(cpt%5 == 0&&cpt%3 == 0){
            printf("fizzbuzz\n");
        }else{
            printf("%d\n",cpt);
        }
    }
    return 0;
}