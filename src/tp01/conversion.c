#include <stdio.h>

float devise = 60;
float taux_change = 1.09;

int main(){
    printf("Alice et Bob ont %.2f euros après conversion.\n", devise*taux_change);
    printf("Ils gardent donc %.2f euros chacun.\n",(devise*taux_change)/2);
    return 0;
}