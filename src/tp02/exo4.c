#include <stdio.h>

void affiche_tableau (char* tableau, int taille);
void affiche_tableau_pointeur (char* tableau, int taille);
void change_lettre (char* pt_lettre, char nouvelle_lettre);

int main(){
    char tableau[6] = {'a','z','e','r','t','y'};
    char* pt_c = &tableau[0];
    affiche_tableau(pt_c,6);
    affiche_tableau_pointeur(pt_c,6);
    change_lettre(pt_c,'w');
    affiche_tableau(pt_c,6);
    affiche_tableau_pointeur(pt_c,6);
    pt_c = &tableau[2];
    change_lettre(pt_c,'w');
    pt_c = &tableau[0];
    affiche_tableau(pt_c,6);
    affiche_tableau_pointeur(pt_c,6);
    return 0;
}

void affiche_tableau (char* tableau, int taille){
    int a;
    for(a=0;a<taille;a++){
        printf("%c ",tableau[a]);
    }
    printf("\n");    
}

void affiche_tableau_pointeur (char* tableau, int taille){
    int a;
    for(a=0;a<taille;a++){
        printf("%c ",*(tableau+a));
    }
    printf("\n");
}

void change_lettre (char* pt_lettre, char nouvelle_lettre){
    *pt_lettre=nouvelle_lettre;
}