#include <stdio.h>

void affiche_caractere (char car);
void alphabet (char* tableau, int taille);
void alphabet_renverse (char* tableau, int taille);
void affiche_tableau_pointeur (char* tableau, int taille);

int main(){
    char tableau[26];
    char* pt_c = &tableau[0];
    affiche_caractere('a');
    affiche_caractere('z');
    affiche_caractere('e');
    affiche_caractere('r');
    affiche_caractere('t');

    alphabet(pt_c,13);
    printf("%s\n",tableau);
    affiche_tableau_pointeur(pt_c,13);
    alphabet_renverse(pt_c,13);
    affiche_tableau_pointeur(pt_c,13);

    return 0;
}

void affiche_caractere (char car){
    printf("En caractere : %c\n", car);
    printf("En entier : %d\n\n", car);
}

void alphabet (char* tableau, int taille){
    int a;
    for(a=0;a<taille;a++){
        if(a==taille/2){
            *(tableau+a)='\0';
        }else{
            *(tableau+a)='a'+a;
        }
    }
    printf("\n");
}

void alphabet_renverse (char* tableau, int taille){
    int a;
    for(a=0;a<taille;a++){
        *(tableau+a)='z'-a;
    }
    printf("\n");
}

void affiche_tableau_pointeur (char* tableau, int taille){
    int a;
    for(a=0;a<taille;a++){
        printf("%c ",*(tableau+a));
    }
    printf("\n");
}