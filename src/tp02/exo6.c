#include <stdio.h>

char valeur_tableau_2d (char* tableau, int nb_ligne, int nb_colonne, int pos_ligne, int pos_colonne);
void affiche_tableau_2d (char* tableau, int nb_ligne, int nb_colonne);
void place_valeur_tableau_2d (char* tableau, int nb_ligne, int nb_colonne, int pos_ligne, int pos_colonne, char car);

int main()
{
    char tableau[16] = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p'};
    char* pt_c = &tableau[0];
    char val;
    affiche_tableau_2d(pt_c,4,4);
    val=valeur_tableau_2d(pt_c,4,4,2,3);
    printf("%c\n",val);
    place_valeur_tableau_2d(pt_c,4,4,2,3,'u');
    affiche_tableau_2d(pt_c,4,4);
    return 0;
}

char valeur_tableau_2d (char* tableau, int nb_ligne, int nb_colonne, int pos_ligne, int pos_colonne){
    return tableau[pos_ligne*nb_colonne+pos_colonne];
}

void affiche_tableau_2d (char* tableau, int nb_ligne, int nb_colonne){
    for (int i = 0; i < nb_ligne; i+=1){
        for(int j = 0; j < nb_colonne; j+=1){
            printf("%c ", valeur_tableau_2d(tableau,4,4,i,j));
        }
        printf("\n");
    }
}

void place_valeur_tableau_2d (char* tableau, int nb_ligne, int nb_colonne, int pos_ligne, int pos_colonne, char car){
    tableau[pos_ligne*nb_colonne+pos_colonne]=car;
}