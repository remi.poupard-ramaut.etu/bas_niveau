#include <stdio.h>

void somme_et_produit (int a, int b, int* pt_somme, int* pt_produit);

int main(){
    int a,b,val_s,val_p;
    a=10;
    b=20;
    val_s=0;
    val_p=0;
    int* pt_s=&val_s;
    int* pt_p=&val_p;
    somme_et_produit(a,b,pt_s,pt_p);
    printf("valeur pt_s : %d\n",*pt_s);
    printf("valeur pt_p : %d\n",*pt_p);
    printf("valeur a : %d\n",a);
    printf("valeur b : %d\n",b);
    printf("valeur val_s : %d\n",val_s);
    printf("valeur val_p : %d\n",val_p);
    return 0;
}

 void somme_et_produit (int a, int b, int* pt_somme, int* pt_produit){
    *pt_somme=a+b;
    *pt_produit=a*b;
 }