#include <stdio.h>

void affiche_entier (int n);
int produit (int a, int b);
void modifie_a (int a);

int main(){
    int a;
    affiche_entier(54);
    affiche_entier(1);
    affiche_entier(25);
    affiche_entier(-45);
    affiche_entier(36);
    a=produit(5, 5);
    affiche_entier(a);
    a=produit(2,8);
    affiche_entier(a);
    a=produit(9,7);
    affiche_entier(a);
    a=produit(4,6);
    affiche_entier(a);
    a=produit(3,8);
    affiche_entier(a);
    modifie_a(a);
    printf("%d\n",a);
    return 0;
}

void affiche_entier (int n){
    printf("La fonction vous affiche l’entier : %d\n",n);
}

int produit (int a, int b){
    return a*b;
}

void modifie_a (int a){
    a+=1;
    printf("a modifie -> %d\n",a);
}